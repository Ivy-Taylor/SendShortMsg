package com.example.lenovo.sendshortmsg;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Administrator on 2017/10/16.
 */

public class ReplyActivity extends AppCompatActivity {
    private ListView lv_list;
    private String[] sms = {"稍后给你打电话","现在无法接听，请问有事吗？","现在无法回复，请稍后再打给我。"};
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);

        lv_list = (ListView) findViewById(R.id.lv_list);
        ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(),R.layout.item_reply,sms);
        lv_list.setAdapter(adapter);
        lv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String content = sms[position];

                Intent intent = new Intent();
                intent.putExtra("content",content);
                setResult(4,intent);

                finish();
            }
        });
    }
}
