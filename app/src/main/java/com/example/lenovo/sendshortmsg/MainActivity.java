package com.example.lenovo.sendshortmsg;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button btn_contact;
    private Button btn_reply;
    private Button btn_send;
    private EditText et_number;
    private EditText et_reply;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_contact = (Button) findViewById(R.id.btn_contact);
        btn_reply = (Button) findViewById(R.id.btn_reply);
        btn_send = (Button) findViewById(R.id.btn_send);
        et_number = (EditText) findViewById(R.id.et_number);
        et_reply = (EditText) findViewById(R.id.et_reply);
    }

    public void click(View v) {
        switch (v.getId()){
            case R.id.btn_contact:
                Intent intent = new Intent(getApplicationContext(),ContactActivity.class);
                //startActivity(intent);
                //  请求码为1===========================第一步==========================
                startActivityForResult(intent,1);
                break;
            case R.id.btn_reply:
                Intent intent1 = new Intent(getApplicationContext(),ReplyActivity.class);
                startActivityForResult(intent1,3);
                break;
            case R.id.btn_send:     //发短信一定要  SEND_SMS 权限
                //  所有的manger都不能new   要使用静态方法    四大组件都不能new
                SmsManager manager = SmsManager.getDefault();

                //  接收短信的号码
                String destinationAddress = et_number.getText().toString().trim();
                //  设置短信服务中心的号码，使用默认号码传null
                String scAddress = null;
                //  具体要发送的内容
                String text = et_reply.getText().toString().trim();
                //  短信发送成功  和  对方成功接收到信息的  回执
                PendingIntent sentIntent = null;
                PendingIntent deliveryIntent = null;
                //  发送文本信息
                manager.sendTextMessage(destinationAddress,scAddress,text,sentIntent,deliveryIntent);
                break;
        }
    }

    /**这个方法要处理多个页面返回的数据，需要通过requestCode | resultCode  来switch区分处理
     * 当开启的Activity关闭之后（finish），就会走这个方法====================第二步==============
     * @param requestCode
     * @param resultCode
     * @param data  就是下个页面的setResult方法传递回来的意图，可以通过这个意图获取对应的数据
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null)
            return;
        switch (requestCode){
            case 1:
                String phone = data.getStringExtra("phone");
                et_number.setText(phone);
                break;
            case 3:
                String content = data.getStringExtra("content");
                et_reply.setText(content);
                break;
        }

    }
}
